import argparse
import datetime


def process_date(datetime_string):
    dt = datetime.datetime.strptime(datetime_string, '%Y-%m-%d %H:%M:%S')
    return dt.strftime('%d-%m-%Y')


def process(line):
    splits = line.split(',')
    values = [process_date(splits[0]), splits[2], splits[3], splits[4]]
    return ','.join(f'\"{value}\"' for value in values)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert EUFY export file to FitBit export file')
    parser.add_argument('-i', help='path to EUFY input file')
    parser.add_argument('-o', help='path to FitBit output file')
    args = parser.parse_args()
    with open(args.i, 'r') as f:
        lines = f.readlines()
    lines = ['Body', 'Date,Weight,BMI,Fat'] + [process(line) for line in lines[1:]]
    with open(args.o, 'w') as f:
        f.writelines('\n'.join(lines))
    print('done')
