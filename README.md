# eufy-to-garmin

Script in order to convert the [EUFY](https://us.eufy.com/products/t9146?ref=navimenu_3_img) export file to a [Fitbit](https://help.fitbit.com/articles/en_US/Help_article/1133.htm?Highlight=TCX%20file) export file format.
This is necessary in order to import those weight data into the [Garmin](https://connect.garmin.com/) account.

## How to

1. Export your Eufy data to CSV
2. Download your data
3. Run the following command line `python main.py -i [input path] -o [output path]`
4. Upload the output file to Garmin Connect using the date format dd-mm-yyyy
